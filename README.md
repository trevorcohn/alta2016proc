# Create ALTA 2016 proceedings

Adapted from [git@bitbucket.org:benhachey/alta15proc.git](ALTA 2015) instructions.

You'll need to install a few packages, e.g.,
```
sudo apt-get install pdftk poppler-utils
```

### Step 1

Create a list of the papers in the conference, comprising a tab-separated file with
*paper_id, authors* and *title* fields. The *paper_id* is a number used to find the 
relevant PDF file, i.e., the number assigned to the part in EasyChair. This can be
done by hand with cut-and-paste from the EasyChair status page and light manual 
editing.

Split the papers by type to create *longpapers.txt*, *shortpapers.txt* and *sharedtaskpapers.txt*.

You will also need to create corresponding *XXXorder.txt* files which contain a
sequence of *paper_id* values corresponding to the order of presentation of the papers.
This might just use *cut* to extract the *paper_id* column from the *XXXpapers.txt* file 
if the files are already in a suitable order. E.g.,
```
cut -f 1 sharedtaskpapers.txt  > sharedtaskorder.txt
```


Note that presentations should be omitted, as they do not appear in the proceedings.

### Step 2 

Download the camera ready papers from EasyChair, you can do this as a zip file from 
the Submissions page (it will include the rejected papers and presentations, but these
will be ignored). Extract this into a new *final/* sub-directory.

Note that the process from here on (modulo the hand editing of *Proceedings.text*) is captured in the convenience script
```
./compile.tex
```
which creates the big pdf file, cuts it into parts, creates the bibliography files and the html page.

Annotate the papers files with an initial column of page counts, using the scripts:
```
./scripts/pages.sh > page_counts.txt
./scripts/prepend_page_counts.sh longpapers.txt{,.pc}
./scripts/prepend_page_counts.sh shortpapers.txt{,.pc}
./scripts/prepend_page_counts.sh sharedtaskpapers.txt{,.pc}
```
This was developed on a Mac; not sure if the command line for interrogating PDFs is available on other platforms.

### Step 3

Run the scripts to create the latex files for each section of the proceedings:
```
mkdir tex
perl scripts/proc.pl longpapers.txt.pc longorder.txt > tex/longpapers.tex
perl scripts/proc.pl shortpapers.txt.pc shortorder.txt > tex/shortpapers.tex
perl scripts/proc.pl sharedtaskpapers.txt.pc sharedtaskorder.txt > tex/sharedtaskpapers.tex
```
Note the year 2016 is hardcoded in the scripts.

### Step 4

Edit the *Proceedings.tex* file. This is a bit involved, and will take a few
hours. You'll need to update the year, committee list, preface, schedule and
list of tutorials and invited speakers. Then you'll be in a position to run
*pdflatex Proceedings* to obtain the proceedings pdf file.

Regarding the schedule, this was done in a Google spreadsheet and exported
in a tsv file (from 'Programme' tab of ALTA16 document), then automatically 
converted into latex source for table rows as follows:
```
python scripts/tsv_to_latex.py 
cut -f 2,3 shortpapers.txt | sed 's/\t/\\\\ \& \\emph{/g; s/$/} \\\\/; s/^/\& /'
```
The first line outputs the contents for the tables on the two workshop days,
which can be cut and paste into the Proceedings file, and the second line gives
a list of short papers for pasting into the table for lightning talks (without
explicit times listed). Note that the order file is ignored, I assume that these
are in the default order of *paper_id*.

### Step 5
Munge the aux files to produce the table of contents
```
mkdir -p anth/ALTA/bib anth/ALTA/pdf
python scripts/toc_to_toc_aux.py > Proceedings.toc_aux
```
Note hardcoded year and number of pages of frontmatter in python script.


### Step 6
Build anthology contents, after updating both scripts for various parameters
```
perl scripts/proc2.pl
perl scripts/proc3.pl 
```
This appears to hang on my mac, perhaps due to PDFtk being flakey on a modern version of OS X. It works fine on Ubuntu.

### Step 7
```
zip -r U16.zip U16/
```
Send to Min Yan Kan. Also needed are signed copyright forms from one author of every paper, eugh.

import sys, re
from datetime import datetime
from time import strptime

print r"""
%!TEX encoding = UTF-8 Unicode
\documentclass[a4paper,11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{fourier} % Utilisation des polices texte
\usepackage{tikz}
\usepackage{verbatim}
\usepackage[active,tightpage]{preview}
\PreviewEnvironment{tikzpicture}
\setlength\PreviewBorder{5pt}%
\usetikzlibrary[positioning]
\usetikzlibrary{patterns}
\usetikzlibrary{arrows,backgrounds,snakes}

% Adapted from example by Laurent Dutriaux

\newcommand{\daywidth}{6 cm}
\begin{document}

\begin{tikzpicture}[x=\daywidth, y=-1cm, node distance=0 cm,outer sep = 0pt]
% Style for Days
\tikzstyle{day}=[draw, rectangle,  minimum height=1cm, minimum width=\daywidth, fill=yellow!20,anchor=south west]
% Style for hours
\tikzstyle{hour}=[draw, rectangle, minimum height=1cm, minimum width=1.5 cm, fill=yellow!30,anchor=north east]

% Styles for events
% Duration of sequences
\tikzstyle{hours}=[rectangle,draw, minimum width=\daywidth, anchor=north west,text centered,text width=15 em]
% Positioning labels for days and hours
\node[day] (mon) at (1,9) {Monday 4$^{th}$};
\node[day] (tue) [right = of mon] {Tuesday 5$^{th}$};
\node[day] (wed) [right = of tue] {Wednesday 6$^{th}$};
\node[hour] (9-10) at (1,9) {9-10};
\node[hour] (10-11) [below= of 9-10] {10-11};
\node[hour] (11-12) [below = of 10-11] {11-12};
\node[hour] (12-13) [below  = of 11-12] {12-13};
\node[hour] (13-14) [below = of 12-13] {13-14};
\node[hour] (14-15) [below = of 13-14] {14-15};
\node[hour] (15-16) [below = of 14-15] {15-16};
\node[hour] (16-17) [below = of 15-16] {16-17};
\node[hour] (17-18) [below = of 16-17] {17-18};
% Draw 15 min and 30 min ticks
\foreach \x in {9,...,17} 
{  \draw (0.98,\x+0.25) -- (1,\x+0.25);
  \draw (0.96,\x+0.5) -- (1,\x+0.5);
  \draw (0.98,\x+0.75) -- (1,\x+0.75); }
\node[hour] (9-10r) [below right = of wed] {9-10};
\node[hour] (10-11r) [below = of 9-10r] {10-11};
\node[hour] (11-12r) [below = of 10-11r] {11-12};
\node[hour] (12-13r) [below = of 11-12r] {12-13};
\node[hour] (13-14r) [below = of 12-13r] {13-14};
\node[hour] (14-15r) [below = of 13-14r] {14-15};
\node[hour] (15-16r) [below = of 14-15r] {15-16};
\node[hour] (16-17r) [below = of 15-16r] {16-17};
\node[hour] (17-18r) [below = of 16-17r] {17-18};
%\draw[step=1cm] (0,7) grid (10,19);
% Draw 15 min and 30 min ticks
\foreach \x in {9,...,17} 
{  \draw (4,\x+0.25) -- (4.02,\x+0.25);
  \draw (4,\x+0.5) -- (4.04,\x+0.5);
  \draw (4,\x+0.75) -- (4.02,\x+0.75); }

"""

with open('programme.tsv') as fin:
    day = None
    start = None
    title = None
    ltyp = None
    
    for line in fin:
        line = line.strip()
        if line:
            parts = line.split('\t')
        
            if parts[0].lower() in ['monday', 'tuesday', 'wednesday']:
                day = parts[0]
                if parts[0].lower()  == 'monday': dom = 5
                elif parts[0].lower() == 'tuesday': dom = 6
                else: dom = 7
            else:
                #print parts
                typ = parts[1].split()[0].lower()
                if typ in ['session', 'tutorial', 'morning', 'afternoon', 'lunch', 'end', 'business', 'best', 'alta']:
                    if ltyp == 'business' and typ == 'best':
                        title = title + r' \& Awards' 
                        continue

                    if start != None:
                        fill = 'green!10'
                        if ltyp == 'session':
                            fill = 'blue!10'
                        elif ltyp == 'tutorial':
                            fill = 'red!10'
                        elif ltyp in ['morning', 'afternoon', 'lunch']:
                            fill = 'white'

                        date0 = datetime(*(strptime('2016/12/%d 0:00' % dom, '%Y/%m/%d %H:%M')[0:6]))
                        sdate = datetime(*(strptime('2016/12/%d ' % dom + start, '%Y/%m/%d %H:%M')[0:6]))
                        edate = datetime(*(strptime('2016/12/%d ' % dom + parts[0], '%Y/%m/%d %H:%M')[0:6]))

                        begin = (sdate - date0).seconds / (60.0 * 60.0)
                        elapsed = (edate - sdate).seconds / (60.0 * 60.0)
                        print r'\node[hours,minimum height=%fcm,fill=%s] at (%d,%f) {\small %s};' \
                          % (elapsed, fill, dom-4, begin, title)
                        #print 'day', day, 'from', start, 'to', parts[0], 'item', title


                    start = parts[0]
                    title = parts[1]
                    ltyp = typ
        else:
            day = None
            start = None
            title = None
            ltyp = None
    

                

print r"""
\end{tikzpicture}
\end{document} 
"""
